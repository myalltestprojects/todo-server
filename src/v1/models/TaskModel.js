const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  owner: String,
  text: String,
  completed: { type: Boolean, default: false },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() }
});

module.exports = mongoose.model('tasks', TaskSchema, 'tasks');
