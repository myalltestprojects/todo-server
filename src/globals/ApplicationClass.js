class Application {

  constructor() {
    this._routes = {};
    this._version = '';
  }

  set version(value) {
    this._version = value;
  }

  get routes() {
    return this._routes;
  }

  get version() {
    return this._version;
  }

}

module.exports = Application;
