const express = require('express');

const userPolice = require('../policies/userPolice');
const users = require('./user');
const userController = require('../controllers/user');
const responses = require('./responses');

const router = express.Router();

router.use(responses);
router.use('/users/auth', userController.login);
router.use('/users/register', userController.register);
router.use('/users/all', userController.all);
router.use('/users', userPolice, users);

router.use((req, res, next) => {
  const err = new Error('Not Found');
  err.codeName = 'ERR_URL_NOT_FOUND';
  err.status = 404;
  return res.badRequest(err);
});

router.use((err, req, res) => {
  res.status(err.status || 500);
  return res.badRequest(err);
});

module.exports = router;
