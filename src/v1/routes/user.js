const express = require('express');

const userController = require('../controllers/user');
const taskController = require('../controllers/task');
const routes = express.Router({ mergeParams: true });


function nestedRoutes(path, configure) {
  const router = express.Router({ mergeParams: true });
  this.use(path, router);
  configure(router);
  return router;
}

express.application.prefix = nestedRoutes;
express.Router.prefix = nestedRoutes;

routes.prefix('/', (user) => {

  user.get('/', (req, res) => {
    res.send('USER API');
  });

  user.prefix('/tasks', (tasks) => {
    tasks.post('/', taskController.create);
    tasks.get('/', taskController.all);
    tasks.put('/', taskController.update);
  });

});


module.exports = routes;
