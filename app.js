const express = require('express');

const config = require('./config/config.json');

const app = express();
const myexpress = require('./config/express')(app);

app.listen(config.app.port, console.log(`Todo service app listening on port ${config.app.port}!`));

