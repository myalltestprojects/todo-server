const crypto = require('crypto');

class PasswordService {


  static createHash(password) {
    return this.md5(password);
  }

  static checkPassword(hash, password) {
    const validHash = this.md5(password);
    return hash === validHash;
  }

  static md5(string) {
    return crypto.createHash('md5').update(string).digest('hex');
  }
}

module.exports = PasswordService;
