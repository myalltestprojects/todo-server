const JwtService = require('../services/JwtService');
const UserModel = require('../models/UserModel');

module.exports = (req, res, next) => {
  if (!req.headers || !req.headers.token) return res.badRequest({
    codeName: 'ERR_BAD_TOKEN',
    message: 'Invalid token',
  });
  const token = req.headers.token;

  return JwtService.verify(token, (err, tokenP) => {

    return UserModel.findOne({ token, _id: tokenP.id }).exec((findError, user) => {
      if (findError || !user) return res.badRequest({ codeName: 'ERR_BAD_TOKEN', message: 'Invalid token' });
      req.userId = user._id;
      req.token = tokenP;
      return next();
    });
  });
};
