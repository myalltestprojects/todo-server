/**
 * @class SchemaValidationService
 */
const _ = require('lodash')._;
const Validator = require('jsonschema').Validator;

const v = new Validator();


/**
 *  json schema validation
 * @param object
 * @param validationSchema
 * @return error = {valid,friendlyMsg,codeName}
 */
function validate(object, validationSchema) {
  const valid = v.validate(object, validationSchema);
  let error = { valid: valid.valid, message: '', codeName: 'ERR_REQUIRED_DATA_NOT_FULL' };
  _.each(valid.errors, (err) => {
    console.error('error:', err.stack);
    error.message = error.message.concat(err.stack).concat(' \n ');
  });
  return error;
}
module.exports = validate;

