const express = require('express');
const start = require('./start.js');

const router = express.Router();

router.use('/swagger-start', (req, res) => res.json(start));

router.use('/', (req, res) => {
  res.redirect('/swagger-ui?url=/doc/v1/swagger-start');
});

module.exports = router;
