const express = require('express');

const router = express.Router();

router.use((req, res, next) => {

  res.ok = (data) => {
    res.json(data);
    res.end();
  };

  res.badRequest = (err, data = {}) => {
    res.status(400);
    res.json(err);
    res.end();
  };

  next();
});

module.exports = router;
