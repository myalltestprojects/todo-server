const config = require('../../../config/config.json');

const paths = require('./paths.js');
const tag = require('./tags.js');

module.exports = {
  swagger: '2.0',
  info: {
    description: 'Agro portal API documentation',
    version: '0.1.0', // <--- swagger version here !
    title: 'Swagger',
  },
  host: config.swagger.host,
  basePath: '/v1',  // <--- API version here !
  tags: tag,
  schemes: [
    'http',
  ],
  consumes: [
    'application/json',
  ],
  produces: [
    'application/json',
  ],
  paths,
};
