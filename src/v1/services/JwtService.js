/**
 * @class JwtService
 */
const jwt = require('jsonwebtoken');
const _ = require('lodash')._;

const settings = require('../settings/token.json');

/**
 * @name: JwtService
 */
class JwtService {

  /**
   * JwtService constructor
   * @param secretKey
   * @param expire
   */
  constructor(secretKey, expire) {
    this.secterKey = secretKey;
    this.expire = expire;
  }

  /**
   * Generates a token from supplied payload
   * @param payload
   * @returns {*}
   */
  issue(payload) {
    return jwt.sign(payload, this.secterKey, { expiresIn: this.expire });
  }

  /**
   * Verify token
   * @param token
   * @param callback
   * @returns {*}
   */
  verify(token, callback) {
    jwt.verify(token, this.secterKey, {}, callback);
  }

  /**
   * Decode token
   * @param token
   * @returns {*}
   */
  decode(token) {
    return jwt.decode(token);
  }

  /**
   * Generate refreshToken without JsonWebToken
   * @returns {string}
   */
  generateRefreshToken() {
    let refreshToken = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 60; i += 1) {
      refreshToken += possible.charAt(_.random(0, possible.length));
    }

    return refreshToken;
  }
}

/**
 * Export JwtService class
 * @type {JwtService}
 */
module.exports = new JwtService(settings.secretKey, settings.expire);

