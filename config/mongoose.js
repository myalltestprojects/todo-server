const mongoose = require('mongoose');
const config = require('./config.json');

mongoose.Promise = global.Promise;
const options = {
  autoIndex: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  poolSize: 10,
  bufferMaxEntries: 0,
  useNewUrlParser: true,
};
mongoose.connect(config.db.mongodb, options);

module.exports = mongoose;
