const routes = require('./routes/middleware');
const Application = require('../globals/ApplicationClass');

class Version extends Application {
  constructor(app) {
    super();
    this._routes = app.routes;
    this._version = app.version;
  }
}

const alpha = new Version({
  version: 'v1',
  routes,
});
module.exports = alpha;

