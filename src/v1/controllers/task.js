const validationService = require('../services/SchemaValidationService');
const TaskModel = require('../models/TaskModel');

module.exports.create = function (req, res) {
  const schema = {
    type: 'object',
    properties: {
      body: {
        properties: {
          text: { type: 'string' }
        },
        required: ['text'],
      },
    },
  };
  const error = validationService(req, schema);
  if (!error.valid) {
    return res.badRequest(error);
  }

  let body = {
    text: req.body.text,
    owner: req.userId,
    createdAt: new Date(),
    updatedAt: new Date()
  };

  let task = new TaskModel(body);

  task.save(function (error3, tsk) {
    if (error3) return res.badRequest(error3);
    return res.ok(tsk);
  });

};

module.exports.update = function (req, res) {
  const schema = {
    type: 'object',
    properties: {
      body: {
        properties: {
          completed: { type: 'boolean' },
          text: { type: 'string' },
          _id: { type: 'string' }
        },
        required: ['text'],
      },
    },
  };
  const error = validationService(req, schema);
  if (!error.valid) {
    return res.badRequest(error);
  }

  let body = {
    text: req.body.text,
    completed: req.body.completed,
    _id: req.body._id,
    updatedAt: new Date()
  };

  TaskModel.updateOne(
      { _id: body._id },
      {
        $set: { completed: body.completed, updatedAt: body.updatedAt }
      }
    ).exec(function (error3, tsk) {
    if (error3) return res.badRequest(error3);
    return res.ok(tsk);
  });

};

module.exports.all = function (req, res) {

  TaskModel.find({owner: req.userId}).sort({createdAt: -1}).exec((err, tasks) => {
    if (err) {
      err.codeName = 'ERR_GET_TASKS';
      return res.badRequest(err);
    }
    if (!tasks) {
      const error = {
        message: 'Tasks not found',
      };
      return res.badRequest(err);
    }
    return res.ok(tasks);
  });
};

