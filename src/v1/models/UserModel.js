const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  login: { type: String, unique: true, required: true },
  name: String,
  password: { type: String, required: true },
  token: String,
  refreshToken: String,
});
UserSchema.index({ login: 1, token: 1 });

module.exports = mongoose.model('users', UserSchema, 'users');
