const validationService = require('../services/SchemaValidationService');
const JwtService = require('../services/JwtService');
const PasswordService = require('../services/PasswordService');
const UserModel = require('../models/UserModel');

module.exports.login = function (req, res) {
  const schema = {
    type: 'object',
    properties: {
      body: {
        properties: {
          login: { type: 'string' },
          password: { type: 'string' },
          rememberMe: { type: 'boolean' },
        },
        required: ['login', 'password'],
      },
    },
  };
  const error = validationService(req, schema);
  if (!error.valid) {
    return res.badRequest(error);
  }

  const body = {
    login: req.body.login,
    password: req.body.password,
    rememberMe: req.body.rememberMe,
  };

  UserModel.findOne({ login: body.login }).exec((err, user) => {
    if (err) {
      err.codeName = 'ERR_GET_USER';
      return res.badRequest(err);
    }
    if (!user) {
      const error1 = {
        message: 'USER not found',
      };
      return res.badRequest(error1);
    }

    if (!PasswordService.checkPassword(user.password, body.password)) {
      const error2 = {
        message: 'Password is incorrect',
      };
      return res.badRequest(error2);
    }

    body.token = JwtService.issue({ id: user._id});

    UserModel.findOneAndUpdate({ _id: user._id }, { $set: {token: body.token} }, (err) => {
      if (err) return res.badRequest(err);
    });

    const response = {
      name: user.name,
      token: body.token
    };
    return res.ok(response);
  });
};

module.exports.register = function (req, res) {
  const schema = {
    type: 'object',
    properties: {
      body: {
        properties: {
          login: { type: 'string' },
          password: { type: 'string' },
          confirmPassword: { type: 'string' },
          name: { type: 'string' },
        },
        required: ['login', 'password', 'confirmPassword', 'name'],
      },
    },
  };
  const error = validationService(req, schema);
  if (!error.valid) {
    return res.badRequest(error);
  }

  const body = {
    login: req.body.login,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    name: req.body.name,
  };

  if(body.password !== body.confirmPassword){
    return res.badRequest({message: 'Password confirmation failed'});
  }
  body.password = PasswordService.md5(body.password);

  let user = new UserModel({ login: body.login, password: body.password, name: body.name });

  user.save(function (error3, usr) {
    if (error3) return res.badRequest(error3);
    return res.ok(usr);
  });

};


module.exports.all = function (req, res) {

  UserModel.find().exec((err, user) => {
    console.log(err, user);
    if (err) {
      err.codeName = 'ERR_GET_USER';
      return res.badRequest(err);
    }
    if (!user) {
      const error = {
        message: 'USER not found',
      };
      return res.badRequest(error);
    }
    return res.ok(user);
  });
};

